eMag recruitment hero game
==========
Author: Mihai Amihailesei

**How to use**

Install composer packages, then run ``docker build . -t hero``

When the docker image is built run it with ``docker run hero``