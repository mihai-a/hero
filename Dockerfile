FROM php:7.2-cli

COPY . /usr/src/hero

WORKDIR /usr/src/hero

CMD [ "php", "./fight.php" ]
