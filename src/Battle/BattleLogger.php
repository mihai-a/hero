<?php

namespace Battle;

/**
 * Class BattleLogger
 * @package Battle
 */
class BattleLogger
{
    /**
     * @var string $attacker
     */
    protected $attacker;

    /**
     * @var string $defender
     */
    protected $defender;

    /**
     * @var string $conclusion
     */
    protected $conclusion;

    /**
     * @var int $turn
     */
    protected $turn;

    /**
     * BattleLogger constructor.
     * @param int $turn
     */
    public function __construct(int $turn)
    {
        $this->turn = $turn;
    }

    public function logSkillUse(string $name, string $skill, int $value, string $user)
    {
        switch ($user) {
            case Contestant::ATTACKER:
                $this->attacker = "$name hit with $skill for $value";
                break;
            case Contestant::DEFENDER:
                $this->defender = "$name defended with $skill for $value";
                break;
        }
    }

    public function logAction(string $name, int $value, string $user)
    {
        switch ($user) {
            case Contestant::ATTACKER:
                $this->attacker = "$name hit for $value";
                break;
            case Contestant::DEFENDER:
                $this->defender = "$name defended for $value";
                break;
        }
    }

    public function logMiss(string $attacker, string $defender, int $remainingHealth)
    {
        $this->attacker = "$attacker missed.";
        $this->defender = "$defender got lucky.";
        $this->conclusion = "$defender's remaining health: $remainingHealth";
    }

    public function logConclusion(string $defender, int $value, int $remainingHealth)
    {
        $this->conclusion = "$defender got hit for $value, remaining health: $remainingHealth";
    }

    public function logEnd(int $turn, string $victor, string $defeated)
    {
        $this->conclusion .= "\n\nBattle ended on turn $turn, $victor defeated $defeated!";
    }

    /**
     * @return int
     */
    public function getTurn(): int
    {
        return $this->turn;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $indentation = '    ';
        $string = "Turn {$this->turn}:\n";
        $string .= $indentation . $this->attacker . "\n";
        $string .= $indentation . $this->defender . "\n";
        $string .= $indentation . $this->conclusion . "\n";
        return $string;
    }


}