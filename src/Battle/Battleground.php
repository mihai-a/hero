<?php

namespace Battle;

use Exception;

/**
 * Class Battleground
 * @package Battle
 */
class Battleground
{
    /**
     * @var array $contestants
     */
    protected $contestants;

    /**
     * @var int $turn
     */
    protected $turn;

    /**
     * @var Contestant $attacker
     */
    protected $attacker;

    /**
     * @var Contestant $defender
     */
    protected $defender;

    /**
     * @var array $battleLogs
     */
    protected $battleLogs;

    /**
     * @var BattleLogger $turnLog
     */
    protected $turnLog;

    /**
     * Battleground constructor.
     * @param array $contestants
     * @throws Exception
     */
    public function __construct(array $contestants)
    {
        if (is_array($contestants) && count($contestants) === 2) {
            $this->contestants = $contestants;
        }
        else {
            throw new Exception('Incorrect number of contestants given. There can be only two contestants.');
        }
        $this->turn = 0;
    }

    /**
     * @return int
     */
    public function getTurn(): int
    {
        return $this->turn;
    }

    /**
     * Increment current turn.
     */
    public function incrementTurn()
    {
        $this->turn++;
    }


    /**
     * @return Contestant
     */
    public function getAttacker(): Contestant
    {
        return $this->attacker;
    }

    /**
     * @param Contestant $attacker
     */
    public function setAttacker(Contestant $attacker)
    {
        $this->attacker = $attacker;
    }

    /**
     * @return Contestant
     */
    public function getDefender(): Contestant
    {
        return $this->defender;
    }


    /**
     * @param Contestant $defender
     */
    public function setDefender(Contestant $defender)
    {
        $this->defender = $defender;
    }

    /**
     * Switches the attacker and defender roles.
     */
    public function switchAttackerDefender()
    {
        $currentAttacker = $this->getAttacker();
        $currentDefender = $this->getDefender();
        $this->setAttacker($currentDefender);
        $this->setDefender($currentAttacker);
    }

    /**
     * @return array
     */
    public function getBattleLogs(): array
    {
        return $this->battleLogs;
    }

    /**
     * @return BattleLogger|bool
     */
    public function getTurnLog()
    {
        if ($this->turnLog !== null)
            return $this->turnLog;

        return false;
    }

    /**
     * @param BattleLogger $turnLog
     */
    public function setTurnLog(BattleLogger $turnLog)
    {
        $this->turnLog = $turnLog;
    }

    /**
     * @return array
     */
    public function getContestants(): array
    {
        return $this->contestants;
    }

    public function prepareBattle()
    {
        $this->incrementTurn();
        $this->generateTurnLog();
        $this->determineFirstHit();

        print "The battle between " . implode(' and ', $this->getContestants()) . " is about to start!\n";
        print "The first attack is being delivered by " . $this->getAttacker()->getFighter()->getName() . ". ";
        print $this->getAttacker()->getFighter()->getCreatureStatistics();
        print "The defender is " .  $this->getDefender()->getFighter()->getName() . ". ";
        print $this->getDefender()->getFighter()->getCreatureStatistics();
        print "\nLet The fighting begin!\n";
    }

    /**
     * @return bool
     */
    public function processTurn(): bool
    {
        if (self::determineLuck($this->getDefender()->getFighter()->getLuck())) {
            $this->getTurnLog()->logMiss(
                $this->getAttacker()->getFighter()->getName(),
                $this->getDefender()->getFighter()->getName(),
                $this->getDefender()->getFighter()->getHealth()
            );
            print (string) $this->getTurnLog();
            $this->prepareNextTurn();
            return true;
        }

        $attack = $this->getAttacker()->attack($this->getTurnLog());
        $defense = $this->getDefender()->defend($this->getTurnLog());

        $damage = $attack - $defense;
        // Take into account that the defender could defend for all the damage.
        if ($damage < 0) {
            $damage = 0;
        }
        $this->getDefender()->getFighter()->takeDamage($damage);

        $defenderRemainingHealth = $this->getDefender()->getFighter()->getHealth();
        // Set remaining health to 0 in case defender died for nice output.
        $defenderRemainingHealth = $defenderRemainingHealth <= 0 ? $defenderRemainingHealth = 0 : $defenderRemainingHealth;
        $this->getTurnLog()->logConclusion(
            $this->getDefender()->getFighter()->getName(),
            $damage,
            $defenderRemainingHealth
        );

        if ($defenderRemainingHealth === 0) {
            $this->getTurnLog()->logEnd(
                $this->getTurn(),
                $this->getAttacker()->getFighter()->getName(),
                $this->getDefender()->getFighter()->getName()
            );

            print (string) $this->getTurnLog();
            return false;
        }

        print (string) $this->getTurnLog();
        $this->prepareNextTurn();

        return true;
    }

    public function determineVictor()
    {
        $attackerHealth = $this->getAttacker()->getFighter()->getHealth();
        $defenderHealth = $this->getDefender()->getFighter()->getHealth();
        $attackerName = $this->getAttacker()->getFighter()->getName();
        $defenderName = $this->getDefender()->getFighter()->getName();
        if ($attackerHealth > $defenderHealth) {
            print "$attackerName is victorious! Defeated $defenderName with $attackerHealth remaining health.\n";
        }
        else if ($attackerHealth < $defenderHealth) {
            print "$defenderName is victorious! Defeated $attackerName with $defenderHealth remaining health.\n";
        }
        else {
            print "The battle ended in a draw. Both $attackerName and $defenderName have $attackerHealth remaining.\n";
        }
    }

    private function prepareNextTurn()
    {
        $this->saveTurnLog();
        $this->incrementTurn();
        $this->generateTurnLog();
        $this->switchAttackerDefender();
    }

    private function generateTurnLog()
    {
        if (!$this->getTurnLog() instanceof BattleLogger
            || $this->getTurnLog()->getTurn() !== $this->getTurn()
        ) {
            $logger = new BattleLogger($this->getTurn());
            $this->setTurnLog($logger);
        }
    }

    private function saveTurnLog()
    {
        $this->battleLogs[$this->getTurn()] = $this->getTurnLog();
    }

    private function determineFirstHit()
    {
        /** @var Contestant $contestantOne */
        $contestantOne = $this->getContestants()[0];
        /** @var Contestant $contestantTwo */
        $contestantTwo = $this->getContestants()[1];

        // Check speeds.
        if ($contestantOne->getFighter()->getSpeed() > $contestantTwo->getFighter()->getSpeed()) {
            $this->setAttacker($contestantOne);
            $this->setDefender($contestantTwo);
        }
        else if ($contestantOne->getFighter()->getSpeed() < $contestantTwo->getFighter()->getSpeed()) {
            $this->setAttacker($contestantTwo);
            $this->setDefender($contestantOne);
        }
        // If the speed is equal, the first attacker is the one with the highest luck.
        else {
            if ($contestantOne->getFighter()->getLuck() > $contestantTwo->getFighter()->getLuck()) {
                $this->setAttacker($contestantOne);
                $this->setDefender($contestantTwo);
            }
            else {
                $this->setAttacker($contestantTwo);
                $this->setDefender($contestantOne);
            }
        }
    }

    /**
     * @param int $percentage
     * @return bool
     */
    public static function determineLuck(int $percentage): bool
    {
        $randomLuckChance = rand(0, 100);

        return $randomLuckChance <= $percentage;
    }
}