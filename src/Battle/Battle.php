<?php

namespace Battle;

use Symfony\Component\Yaml\Yaml;
use Unit\Properties\Skill;
use Unit\Properties\Skill\Action;
use Exception;

/**
 * Class Battle
 * @package Unit\Battle
 */
class Battle
{

    /**
     * @var int
     */
    protected $maxTurns;

    /**
     * @var array
     */
    protected $contestants;

    /**
     * @var Battleground
     */
    protected $battleground;

    /**
     * Battle constructor.
     * @param string $ymlPath
     * @throws Exception
     */
    public function __construct(string $ymlPath)
    {
        $yml = Yaml::parseFile($ymlPath);
        $this->maxTurns = $yml['battle']['max_turns'];
        $this->contestants = $yml['battle']['contestants'];
        $this->battleground = $this->buildBattleground();
    }

    public function fight()
    {
        $battleground = $this->getBattleground();
        $battleground->prepareBattle();

        do {
            $turn = $battleground->processTurn();
        } while ($turn && $this->getMaxTurns() >= $battleground->getTurn());
        if ($this->getMaxTurns() == $battleground->getTurn()) {
            $battleground->determineVictor();
        }
    }

    /**
     * @return int
     */
    public function getMaxTurns(): int
    {
        return $this->maxTurns;
    }

    /**
     * @return Battleground
     */
    public function getBattleground(): Battleground
    {
        return $this->battleground;
    }

    /**
     * @return Battleground
     * @throws Exception
     */
    private function buildBattleground(): Battleground
    {
        $contestants = [];
        foreach ($this->contestants as $contestant) {
            $contestants[] = $this->buildContestant($contestant);
        }
        return new Battleground($contestants);
    }

    /**
     * @param string $contestant
     * @return Contestant
     * @throws Exception
     */
    private function buildContestant(string $contestant): Contestant
    {
        $contestant = Yaml::parseFile(CONFIG_DIR . $contestant . '.yml');

        $contestantType = array_keys($contestant);
        $contestantType = $contestantType[0];

        if (!class_exists($contestantType) && !isset($contestant[$contestantType]['stats']))
            throw new Exception('Misconfigured contestant' . $contestant);

        $contestant = $contestant[$contestantType];
        $contestantClass = '\\Unit\\' . $contestantType;
        if (!class_exists($contestantClass))
            throw new Exception("Creature type $contestantType does not exist.");


        $name = $contestant['name'];
        $health = rand($contestant['stats']['min']['health'], $contestant['stats']['max']['health']);
        $strength = rand($contestant['stats']['min']['strength'], $contestant['stats']['max']['strength']);
        $defense = rand($contestant['stats']['min']['defense'], $contestant['stats']['max']['defense']);
        $speed = rand($contestant['stats']['min']['speed'], $contestant['stats']['max']['speed']);
        $luck = rand($contestant['stats']['min']['luck'], $contestant['stats']['max']['luck']);
        $hasSkills = isset($contestant['skills']);
        $creature = new $contestantClass($name, $health, $strength, $defense, $speed, $luck, $hasSkills);

        if ($hasSkills) {
            foreach ($contestant['skills'] as $skillType => $skill) {
                if (!in_array($skillType, Skill::getSkillTypes()))
                    throw new Exception("Skill type $skillType does not exist.");

                $skillAction = new Action($skill);
                $skillDescription = self::getSkillDescription($skill);
                $skillChance = $this->getSkillChance($skillDescription);
                $skill = new Skill($skillType, $skillDescription, $skillChance, $skillAction);
                $skillType = strtoupper($skillType);
                $method = "set{$skillType}Skill";
                $creature->$method($skill);
            }
        }

        return new Contestant($creature);
    }

    /**
     * @param string $skillName
     * @return string
     * @throws Exception
     */
    public static function getSkillDescription(string $skillName): string
    {
        $skillDescriptions = Yaml::parseFile(CONFIG_DIR . 'skill_description.yml');
        if (array_key_exists($skillName, $skillDescriptions))
            return $skillDescriptions[$skillName];

        throw new Exception("Skill $skillName does not have a description.");
    }

    /**
     * @param string $skillDescription
     * @return int
     * @throws Exception
     */
    private function getSkillChance(string $skillDescription): int
    {
        $match = preg_match('/([0-9][0-9]%)/', $skillDescription, $matches);
        if (!$match)
            throw new Exception("The skill description \"$skillDescription\" does not contain the skill chance.");

        $percentage = $matches[0];
        $percentage = str_replace('%', '', $percentage);
        $percentage = (int) $percentage;

        return $percentage;
    }

}