<?php

namespace Battle;

use Unit\Creature;

/**
 * Class Contestant
 * @package Battle\Contestants
 */
class Contestant
{
    const ATTACKER = 'attacker';
    const DEFENDER = 'defender';

    /**
     * @var Creature $contestant
     */
    protected $contestant;

    public function __construct(Creature $contestant)
    {
        $this->contestant = $contestant;
    }

    /**
     * @return Creature
     */
    public function getFighter(): Creature
    {
        return $this->contestant;
    }

    /**
     * @param BattleLogger $logger
     * @return int
     */
    public function attack(BattleLogger $logger): int
    {
        $contestant = $this->getFighter();
        if ($contestant->hasSkills()) {
            if (Battleground::determineLuck($contestant->getOffensiveSkill()->getSkillChance())) {
                $attackValue = $contestant->getOffensiveSkill()->useSkill($contestant->getStrength());
                $logger->logSkillUse(
                    $contestant->getName(),
                    $contestant->getOffensiveSkill()->getSkillName(),
                    $attackValue,
                    Contestant::ATTACKER
                );
                return $attackValue;
            }
        }
        $logger->logAction($contestant->getName(), $contestant->getStrength(), Contestant::ATTACKER);
        return $contestant->getStrength();
    }

    /**
     * @param BattleLogger $logger
     * @return int
     */
    public function defend(BattleLogger $logger): int
    {
        $contestant = $this->getFighter();
        if ($contestant->hasSkills()) {
            if (Battleground::determineLuck($contestant->getDefensiveSkill()->getSkillChance())) {
                $defenseValue = $contestant->getDefensiveSkill()->useSkill($contestant->getDefense());
                $logger->logSkillUse(
                    $contestant->getName(),
                    $contestant->getDefensiveSkill()->getSkillName(),
                    $defenseValue, Contestant::DEFENDER
                );
                return $defenseValue;
            }
        }
        $logger->logAction($contestant->getName(), $contestant->getDefense(), Contestant::DEFENDER);
        return $contestant->getDefense();
    }

    public function __toString()
    {
        return $this->getFighter()->getName();
    }


}