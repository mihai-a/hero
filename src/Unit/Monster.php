<?php

namespace Unit;

/**
 * Class Monster
 * @package Creature
 */
class Monster extends Creature
{
    /**
     * @return string
     */
    public function getCreatureStatistics()
    {
        $statistics = "This monster has the following stats:\n";
        $statistics .= parent::getCreatureStatistics();

        return $statistics;
    }
}