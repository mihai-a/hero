<?php

namespace Unit;

/**
 * Class Hero
 * @package Creature
 */
class Hero extends Creature
{
    /**
     * @return string
     */
    public function getCreatureStatistics()
    {
        $statistics = "{$this->getName()} has the following stats:\n";
        $statistics .= parent::getCreatureStatistics();

        return $statistics;
    }
}