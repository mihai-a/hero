<?php

namespace Unit;

use Unit\Traits\Skills;

/**
 * Class Creature
 * @package Creature
 */
abstract class Creature
{
    use Skills;

    /**
     * @var int $health
     */
    protected $health;

    /**
     * @var int $strength
     */
    protected $strength;

    /**
     * @var int $defense
     */
    protected $defense;

    /**
     * @var int $speed
     */
    protected $speed;

    /**
     * @var float $luck
     */
    protected $luck;

    /**
     * @var bool $hasSkills
     */
    protected $hasSkills;
    /**
     * @var string $name
     */
    protected $name;

    /**
     * Monster constructor.
     * @param string $name
     * @param int $health
     * @param int $strength
     * @param int $defense
     * @param int $speed
     * @param float $luck
     * @param bool $hasSkills
     */
    public function __construct(
        string $name,
        int $health,
        int $strength,
        int $defense,
        int $speed,
        float $luck,
        // Default to no skills.
        bool $hasSkills = false
    ) {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defense = $defense;
        $this->speed = $speed;
        $this->luck = $luck;
        $this->hasSkills = $hasSkills;
    }

    /**
     * @return bool
     */
    public function hasSkills(): bool
    {
        return $this->hasSkills;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    private function setHealth(int $health)
    {
        $this->health = $health;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @return int
     */
    public function getDefense(): int
    {
        return $this->defense;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @return float
     */
    public function getLuck(): float
    {
        return $this->luck;
    }

    /**
     * Gets the monster statistics.
     *
     * @return string
     */
    public function getCreatureStatistics()
    {
        $statistics = "Health: {$this->getHealth()}\n";
        $statistics .= "Strength: {$this->getStrength()}\n";
        $statistics .= "Defense: {$this->getDefense()}\n";
        $statistics .= "Speed: {$this->getSpeed()}\n";
        $statistics .= "Luck: {$this->getLuck()}\n";

        return $statistics;
    }

    public function takeDamage(int $damage)
    {
        $this->setHealth($this->getHealth() - $damage);
    }
}