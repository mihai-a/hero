<?php

namespace Unit\Properties\Skill;

/**
 * This class contains the logic for the skills. If a skill is used, it will be logged by the battle logger, and this
 * class will be responsible for adding the correct functionality to the skill.
 * Define new skills as a constant to be used when constructing an action. Name the action using Title Case, and define
 * new functionality as titleCaseFunctionality(). See existing examples.
 *
 * Class Action
 * @package Creature\Skills\Skill
 */
class Action
{
    const RAPID_STRIKE = 'Rapid Strike';
    const MAGIC_SHIELD = 'Magic Shield';

    /**
     * @var string $name
     */
    protected $name;

    /**
     * Action constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $skill
     * @param int $value
     *
     * @return int
     */
    public function act($skill, $value): int
    {
        $skill = explode(' ', $skill);
        $skill[0] = strtolower($skill[0]);
        $skillMethod = implode($skill, '') . 'Functionality';

        return $this->{$skillMethod}($value);
    }

    /**
     * @param int $damage
     * @return int
     */
    private function rapidStrikeFunctionality(int $damage): int
    {
        return $damage * 2;
    }

    /**
     * @param int $damage
     * @return int
     */
    private function magicShieldFunctionality(int $defense): int
    {
        return $defense * 2;
    }

}