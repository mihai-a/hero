<?php

namespace Unit\Properties;

use Unit\Properties\Skill\Action;

/**
 * Class Skill
 * @package Unit\Properties
 */
class Skill
{
    const DEFENSIVE = 'defensive';
    const OFFENSIVE = 'offensive';

    /**
     * @var string $skillType
     */
    protected $skillType;

    /**
     * @var string $skillDescription
     */
    protected $skillDescription;

    /**
     * @var Action $action
     */
    protected $action;

    /**
     * @var string $skillName
     */
    protected $skillName;

    /**
     * @var int $skillChance
     */
    protected $skillChance;

    public function __construct(string $skillType, string $skillDescription, int $skillChance, Action $action)
    {
        $this->skillType = $skillType;
        $this->skillDescription = $skillDescription;
        $this->skillChance = $skillChance;
        $this->skillName = $action->getName();
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getSkillDescription(): string
    {
        return $this->skillDescription;
    }

    /**
     * @return int
     */
    public function getSkillChance(): int
    {
        return $this->skillChance;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getSkillName(): string
    {
        return $this->skillName;
    }

    /**
     * @param int $incomingValue
     * @return int
     */
    public function useSkill($incomingValue): int
    {
        return $this->getAction()->act($this->getSkillName(), $incomingValue);
    }

    /**
     * @return array
     */
    public static function getSkillTypes(): array
    {
        return [self::DEFENSIVE, self::OFFENSIVE];
    }
}