<?php

namespace Unit\Traits;

use Unit\Properties\Skill;

/**
 * We're holding the logic for skills inside a trait in case there may be another unit that can inherit skills.
 *
 * Trait Skills
 * @package Unit\Traits
 */
trait Skills
{
    /**
     * @var array $skills
     */
    protected $skills = [];

    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param Skill $skill
     */
    public function setOffensiveSkill(Skill $skill)
    {
        $this->skills[Skill::OFFENSIVE] = $skill;
    }

    /**
     * @param Skill $skill
     */
    public function setDefensiveSkill(Skill $skill)
    {
        $this->skills[Skill::DEFENSIVE] = $skill;
    }

    /**
     * @return Skill|bool
     */
    public function getOffensiveSkill(): Skill
    {
        if ($this->hasSkills() && isset($this->skills[Skill::OFFENSIVE])) {
            return $this->skills[Skill::OFFENSIVE];
        }
        return false;
    }

    /**
     * @return Skill|bool
     */
    public function getDefensiveSkill(): Skill
    {
        if ($this->hasSkills() && isset($this->skills[Skill::DEFENSIVE])) {
            return $this->skills[Skill::DEFENSIVE];
        }
        return false;
    }
}