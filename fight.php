#!/usr/bin/php
<?php

require __DIR__ . '/vendor/autoload.php';

define('CONFIG_DIR', __DIR__ . '/config/');

try {
    $battle = new \Battle\Battle(__DIR__ . '/config/battle.yml');
    $battle->fight();
    print "\n";
} catch (Exception $e) {
    print $e->getMessage();
}
